;(function(window){
  'use strict'

  var bdbAdv = bdbAdv || {};

  bdbAdv = {
      offc: document.querySelector('#off-canvas')
    , main: document.querySelector('div[role="main"]')
    , initialize: function(){
        this.offcanvasListener();
        this.gallery.initialize();

        window.onrezise = function(){
          this.offcanvas();
        };
      }
    , offcanvas: function(){

      if(bdbAdv.main.style.left === '0px'){
        bdbAdv.main.setAttribute('style', 'left: ' + bdbAdv.offc.offsetWidth + 'px');
        bdbAdv.offc.setAttribute('style', 'left: 0');
      }
      else{
        bdbAdv.main.setAttribute('style', 'left: 0');
        bdbAdv.offc.setAttribute('style', 'left: -' + bdbAdv.offc.offsetWidth + 'px');
      }

    }
    , offcanvasListener: function(){
      var icon = document.querySelector('.menu');
      icon.addEventListener('click', this.offcanvas);

      bdbAdv.main.setAttribute('style', 'left: 0');
    }
    , showoff: function(){
      this.offc.setAttribute('style', 'left: -' + this.offc.offsetWidth + 'px');
    }
    , gallery : {
        initialize: function(){
          this.overlay = document.querySelector('.overlay') || null;
          this.lightbox = document.querySelector('.lightbox') || null;
          this.imgs = document.querySelectorAll('#gallery a') || null;

          for (var i = 0; i < this.imgs.length; ++i) {
            this.imgs[i].addEventListener('click', function(e){
              this.showLightbox(e);
            }.bind(this));
          }
          document.querySelector('.close').addEventListener('click', function(){
            this.hideLightbox();
          }.bind(this));
        }
      , overlay: null
      , lightbox: null
      , imgs: null
      , showLightbox: function(e){
          e.preventDefault();

          var img, h4, p;

          img = this.lightbox.querySelector('img');
          h4 = this.lightbox.querySelector('h4');
          p = this.lightbox.querySelector('p');

          img.setAttribute('src', e.currentTarget.getAttribute('href'));
          h4.innerHTML = e.currentTarget.parentNode.querySelector('h4').innerHTML;
          p.innerHTML = e.currentTarget.parentNode.querySelector('p').innerHTML;

          img.onload = function(e){
            this.overlay.setAttribute('style', 'z-index: 10; opacity: .9');
            this.lightbox.setAttribute('style', 'z-index: 11; width:' + e.currentTarget.width + 'px; margin-left: ' + ( document.querySelector('#gallery').offsetWidth - e.currentTarget.width)/2 + 'px; opacity: .9;' );
          }.bind(this);

          
      }
      , hideLightbox: function(){

        var img, h4, p, flag;

        img = this.lightbox.querySelector('img');
        h4 = this.lightbox.querySelector('h4');
        p = this.lightbox.querySelector('p');
        flag = true;

        h4.innerHTML = '';
        p.innerHTML = '';
        this.lightbox.removeChild(img);

        this.overlay.setAttribute('style', 'z-index: -10; opacity: 0;');
        this.lightbox.setAttribute('style', 'z-index: -11; opacity: 0;');

        this.lightbox.insertBefore(document.createElement('img'), h4);

      }
    }
  }

  window.bdbAdv = bdbAdv;
  window.bdbAdv.initialize();

}(window));