$(function($) {

	// gallery
	$(document).ready(function() {
		var title = ''
		, descr = '';
	 
		$('#pictures ul li').each(function(index) {
			title = $(this).find('img').prop('title');
			descr = $(this).find('img').data('description');
			if(title) { $(this).append('<h3>'+title+'</h3>'); }
			if(descr) { $(this).append('<p>'+descr+'</p>'); }
			$(this).find('a').first().addClass('image').prop('rel', 'group') ;
		});

		$('#pictures ul li a').fancybox();
	});


	// menu
	$(document).ready( function() {
	    $('nav#nav').smint({
	    	'scrollSpeed' : 1000
	    });
	});
});

